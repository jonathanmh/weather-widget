import React, { Component } from 'react';

class Form extends Component {
  constructor(props){
    super(props);

    this.state = {
      city: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(changeEvent){
    this.setState({city: changeEvent.target.value});
  }

  handleSubmit(submitEvent){
    submitEvent.preventDefault();
  }

  render() {
    return (
      <form className="form-inline" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <input type="text" className="form-control" id="city" placeholder="City" onChange={this.handleChange} />
        </div>
        <button type="submit" className="btn btn-default">Search</button>
      </form>
    )
  }
}

export default Form;