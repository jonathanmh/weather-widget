import React, { Component } from 'react';

import axios from 'axios';

import './App.css';

import Form from './components/Form.jsx';

class App extends Component {
  constructor(){
    super();
    this.state = {
      city: 'Copenhagen',
      initial: true,
      tempCelsius: 0,
      humidity: 0,
      windSpeed: 0,
      windDirection: ''
    }
  }

  componentWillMount(){
    var self = this;

    axios.request({
      url: 'http://localhost:8000?weather='+this.state.city,
      method: 'GET',
      timeout: 1000,
    })
    .then(function (response) {
      let weather = response.data.payload.main;
      let wind = response.data.payload.wind;

      let windDirectionString = self.windDirectionString(wind.deg);

      self.setState({
        tempCelsius: weather.temp,
        humidity: weather.humidity,
        windSpeed: wind.speed,
        windDirection: windDirectionString
      });

    })
    .catch(function (error) {
    });
  }

  // credits to steve-gregory on stackoverflow: https://stackoverflow.com/questions/7490660/converting-wind-direction-in-angles-to-text-words#7490772
  windDirectionString( degrees ){
    let val = parseInt((degrees/22.5)+.5, 10);
    let arr = ["Nord", "NordNordØst", "NordØst", "ØstNordØst", "Øst", "ØstSydØst", "SydØst", "SydSydØst", "Syd", "SydSydVest", "SydVest", "VestSydVest", "Vest", "VestNordVest", "NordVest", "NordNordVest"];
    return arr[val % 16];
  }

  render() {
    return (
    <div className="panel panel-info">
      <div className="panel-heading">Weather in <b>{this.state.city}</b></div>
      <ul className="list-group">
        <li className="list-group-item">Temperature: <b>{this.state.tempCelsius}°C</b></li>
        <li className="list-group-item">Humidity: <b>{this.state.humidity}</b></li>
        <li className="list-group-item">Wind: <b>{this.state.windSpeed} m/s {this.state.windDirection}</b></li>
        <li className="list-group-item">
          <Form />
        </li>
      </ul>
    </div>
    );
  }
}

export default App;
