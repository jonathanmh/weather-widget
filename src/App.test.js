import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils';

import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

// source: http://snowfence.umn.edu/Components/winddirectionanddegreeswithouttable3.htm
it('transforms degrees into a compass direction', () => {
  const App2 = ReactTestUtils.renderIntoDocument(<App />);
  expect(App2.windDirectionString(120)).toBe('ØstSydØst');
  expect(App2.windDirectionString(123.75)).toBe('SydØst');
  expect(App2.windDirectionString(213.75)).toBe('SydVest');
});
