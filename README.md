# Weather Widget

## installation and development

1. run `npm i`
2. create a `.env` file
3. define the `API_KEY` in `.env`

## starting the app/s

For starting the node process: `node server.js`.

For development mode of the react app: `npm run start` (will open browser tab to port 3000).

## Tests

For tests see: `${filename}.test.js`, currently:

* `App.test.js`

## Questions for Designer / PM:

* How will the wind direction be displayed? North/East/South/West sufficient?
* Do we render the UI before populating with values or wait until initial data has been loaded to avoid stealing the users attention?
* Do we allow plain text search or only clicks on a possible autocomplete?
* Do we want to log results of who searches for what weather when located where?

## Roadmap ;)

* Perform request on user searching
* Implement either React-router or simple navigator/history.pushState city segment in URL that will load the right city when shared as a link
* Implement form route and fallback for the plain HTML version (render component replacing existing element)
* user facing error display

## Nice to have / out of scope

* Autocomplete search of Danish city names, test case: Aarhus/Århus (suspicion: openweatherapi does not properly handle upper/lower cases of special chars)
* Connection retry on failed request
