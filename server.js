// CORE IMPORTS
const http = require('http');
const url = require('url');

// THIRD PARTY
const dotenv = require('dotenv').config();
const request = require('request');

// OWN CONSTANTS
const API_KEY = process.env.API_KEY;
const API_URL_BASE = 'http://api.openweathermap.org/data/2.5';
const COUNTRY_CODE = 'dk'; // ISO 3166
const PORT = 8000;

const weatherRequestHandler = function(request, response) {
  response.setHeader('Access-Control-Allow-Origin', '*');
  parsedURL = url.parse(request.url, true);

  // if the query string is invalid, fail early
  if(isQueryObjectValid(parsedURL.query) === false){
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.end(JSON.stringify({status: 'error'}));
  }

  let city = parsedURL.query.weather;

  queryOpenWeatherCity(city, function(err, apiResponse){
    response.writeHead(200, { 'Content-Type': 'application/json' });
    if(err){
      response.end(JSON.stringify({status: 'error'}));
    }
    response.end(JSON.stringify({status: 'ok', payload: apiResponse}));
  });
}

const queryOpenWeatherCity = function(cityString, callback){

  let cityStringEncoded = encodeURIComponent(cityString)

  request(`${API_URL_BASE}/weather?q=${cityStringEncoded},${COUNTRY_CODE}&appid=${API_KEY}&units=metric`, function (error, response, body) {
    if(error) {
      return callback(error, null);
    }
    console.log(response.req.path)
    return callback(null, JSON.parse(body));
  })
}

const isQueryObjectValid = function(queryObject){
  if(
    Object.keys(queryObject).length === 0
    || typeof queryObject.weather === undefined
    || queryObject.weather.length < 1
  ){
    return false;
  }

  return true;
}

const server = http.createServer(weatherRequestHandler)

server.listen(PORT, (err) => {
  if (err) {
    return console.log('server error:', err)
  }

  console.log(`server is listening on ${PORT}`)
})
